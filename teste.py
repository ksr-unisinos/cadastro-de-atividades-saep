from icalendar import Calendar, Event, vCalAddress, vText, Alarm
import pytz
from datetime import datetime
import os
from pathlib import Path

def handleCalendar(palestras,palestrantes):
	cal = Calendar()
	#cal.add('attendee', 'MAILTO:abc@example.com')
	cal.add('version', 2.0)
	cal.add('calscale', 'GREGORIAN')
	cal.add('method', 'PUBLISH')
	cal.add('x-wr-calname', 'testepython')
	cal.add('x-wr-timezone', 'America/Sao_Paulo')
	i = 0
	for palestra in palestras:
		i+=1
		event = Event()
		event.add('dtstart', datetime(2022, 10, 17+i, 8, 0, 0, tzinfo=pytz.utc))
		event.add('dtend', datetime(2022, 10, 17+i, 10, 0, 0, tzinfo=pytz.utc))
		event.add('dtstamp', datetime(2022, 10, 17+i, 0, 10, 0, tzinfo=pytz.utc))
		
		event.add('description', 'Descrição texto em <b>negrito</b>')
		event.add('location', 'São Leopoldo')
		event.add('sequence',str(i) )
		event.add('status', 'CONFIRMED')
		event.add('summary', 'Python meeting about calendaring')
		event.add('transp', 'OPAQUE')
		
		alarm = Alarm()
		event.add('action', 'DISPLAY')
		event.add('description', 'Lembrete')
		event.add('trigger', datetime(2022, 10, 17+i, 0, 10, 0, tzinfo=pytz.utc))

		#organizer = vCalAddress('MAILTO:hello@example.com')
		#organizer.params['cn'] = vText('Sir Jon')
		#organizer.params['role'] = vText('CEO')
		#event['organizer'] = organizer
		#event['location'] = vText('London, UK')

		# Adding events to calendar
		cal.add_component(event)

	directory = str(Path(__file__).parent.parent) + "/"
	print("ics file will be generated at ", directory)
	f = open(os.path.join(directory, 'example.ics'), 'wb')
	f.write(cal.to_ical())
	f.close()

handleCalendar([0,0,0,0],0)