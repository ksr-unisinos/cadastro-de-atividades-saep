from contato import *
import pandas as pd
from pprint import pprint



hmtlHeader ='''<!-- wp:paragraph -->
<p>Na sétima edição da <a href="https://www.instagram.com/saepunisinos/">SAEP </a>– Semana Acadêmica da Escola Politécnica da Unisinos, teremos uma série de atividades envolvendo todos os cursos da Escola Politécnica. Embora algumas atividades sejam específicas para um curso ou outro, o objetivo da <a href="https://www.instagram.com/saepunisinos/">SAEP </a>é que os acadêmicos possam interagir com outras áreas do conhecimento e abrir suas mentes.</p>
<!-- /wp:paragraph -->

<!-- wp:heading -->
<h2>Lista de Atividades:</h2>
<!-- /wp:heading -->'''



icon={
	'LinkedIn':'https://cdn-icons-png.flaticon.com/512/174/174857.png',
	'Instagram':'https://cdn-icons-png.flaticon.com/512/174/174855.png',
	'Lattes':'http://paginapessoal.utfpr.edu.br/jlrebelatto/icon_Lattest.png',
	'e-mail':'https://cdn-icons-png.flaticon.com/512/561/561127.png',
}

def heading(txt,size=3):
	size = str(size)
	result='''
<!-- wp:heading {"level":'''+size+'''} -->
<h'''+size+'''><strong>'''+txt+'''</strong></h'''+size+'''>
<!-- /wp:heading -->
'''
	return result

def paragraph(txt):
	result = '''
<!-- wp:paragraph -->
<p>'''+txt+'''</p>
<!-- /wp:paragraph -->
'''
	return result

def foto(txt,size=300):
	size = str(size)
	result = '''
<!-- wp:image {"align":"center","width":'''+size+''',"sizeSlug":"large"} -->
<figure class="wp-block-image aligncenter size-large is-resized"><img src="''' + txt['Foto'] + '''" alt="" width="'''+size+'''"/><figcaption class="wp-element-caption">''' +txt['nome'] + '''</figcaption></figure>
<!-- /wp:image -->
'''
	return result

def icone(link,fig,size=38):
	size = str(size)
	result ='''
<!-- wp:image {"align":"center","width":'''+size+''',"height":'''+size+''' ,"sizeSlug":"large","linkDestination":"custom"} -->
<figure class="wp-block-image aligncenter size-large is-resized">
<a href="'''+link+'''" target="_blank" rel="noreferrer noopener">
<img src="''' +fig+ '''" alt="" width="'''+size+'''" height="'''+size+'''"/></a>
</figure>
<!-- /wp:image -->
'''
	return result

startGroup = '''
<!-- wp:group {"layout":{"type":"flex","flexWrap":"nowrap","justifyContent":"center"}} -->
<div class="wp-block-group">
'''
endGroup = '''
</div><!-- /wp:group -->
'''

separator = '''
<!-- wp:separator -->
<hr class="wp-block-separator has-alpha-channel-opacity"/>
<!-- /wp:separator -->
'''

def htmlConcat(data,df):
	plural = ''
	if(len(data['participantes'])>1): plural = 's'
	
	html = heading(data['titulo'])
	html += paragraph(data['resumo'])
	html += paragraph('<strong>Data: </strong>'+data['dia']+' ('+data['semana']+') '+data['horário'])
	html += paragraph('<strong>Local:</strong> <a href="'+data['link']+'" target="_blank" rel="noreferrer noopener">'+data['Nome do Link']+'</a>')

	html += paragraph('<strong>Palestrante' + plural + ':</strong>')

	for Nome in data['participantes']:
		info = handleContatoJson(Nome,df)
		
		if 'Foto' in info:
			html += foto(info)
			if 'Resumo' in info:
				html += paragraph(info['Resumo'])
			
			if 'LinkedIn' in info or 'Instagram' in info or 'Lattes' in info or 'e-mail' in info:
				html += startGroup
				for key in icon:
					if key in info:
						html +=icone(info[key],icon[key])
				html += endGroup + separator
		else:
			html += heading(info['nome']) + separator
			print('->'+info['nome']+'<-')

	return html


def handleHtml(dataArray,palestrantes):
	hmtl = hmtlHeader
	for d in dataArray:
		hmtl += htmlConcat(d,palestrantes)

	with open('site.htm', 'w', encoding='utf8') as f:
			f.write(hmtl)