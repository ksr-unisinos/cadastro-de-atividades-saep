from docx import Document
import pandas as pd


document = Document()

'''
17 de outubro (Segunda-feira) 
Titulo | 20:00 às 22:00 
Descrição: blablabla 
Palestrantes: nomes 
'''

def handleDoc(data,df,semana):
	p = document.add_paragraph('')
	#print(semana[data['dia']])
	p.add_run(data['dia']+' de outubro (' +semana[data['dia']]+')\n').underline = True
	
	
	p.add_run(data['titulo']).bold = True
	p.add_run(' | ' +data['horário'][10:-1]+'\n')
	p.add_run('Descrição: '+data['resumo']+'\n')

	if len(data['participantes']) > 1:
		p.add_run('Palestrantes: ')
	else: p.add_run('Palestrante: ')
	j=len(data['participantes'])
	for i in data['participantes']:
		p.add_run(i)
		j-=1
		if(j): p.add_run(', ')
	return


def handleDocPalestrante(nome,df):
	result =[]
	new = df[df.isin([nome]).any(axis=1)]
	
	p = document.add_paragraph('')
	passaporte = str(new['Passaporte (apenas para estrangeiros)'].values[0])
	if passaporte == 'nan': passaporte = ''
	aniver = str(new['Data de Nascimento'].values[0])
	aniver = aniver[8:10]+'/'+aniver[5:7]+'/'+aniver[:4]
	records = (
		#('Data de Nascimento:', aniver),
		#('Estado Natal:', str(new['Estado Natal'].values[0])),
		#('Local de Nascimento:', str(new['Local de Nascimento'].values[0])),
		#('Sexo:', str(new['Sexo'].values[0])),
		('E-mail:', str(new['e-mail'].values[0])),
		('Celular/WhatsApp:', str(new['Telefone'].values[0])),
		#('CPF:', str(new['CPF'].values[0])),
		#('Passaporte*:',passaporte),
	)

	table = document.add_table(rows=1, cols=2)
	# Adding style to a table
	table.style = 'TableGrid'
	
	row = table.rows[0].cells
	row[0].text = 'Nome Completo:'
	row[1].text = nome
	
	for item, value in records:
		row_cells = table.add_row().cells
		row_cells[0].text = item
		row_cells[1].text = value
	return



def handleDocDF(dataArray,palestrantes,semana,nomesPalestrando):
	document.add_heading('Doc Unisinos', 0)

	for n in dataArray:
		handleDoc(n,palestrantes,semana)
	for n in nomesPalestrando:
		new = palestrantes[palestrantes.isin([n]).any(axis=1)]
		if len(new) > 0:
			temp = str(new['CPF'].values[0])
			if temp != 'nan':
				handleDocPalestrante(n,palestrantes)
	
	document.add_page_break()
	document.save('doc.docx')