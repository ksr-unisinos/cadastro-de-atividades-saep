REM http://timmyreilly.azurewebsites.net/python-pip-virtualenv-installation-on-windows/
@echo off
cls
set Projeto=CadastroSAEP
set file=main.py

echo "%venvs%\%Projeto%\"

IF NOT EXIST "%venvs%\%Projeto%\" (
	echo Projeto %Projeto% nao encontrado
	mkvirtualenv %Projeto%
	setprojectdir
	if exist "requiriments.txt" (
		echo Instala Requerimentos
		pip install -r requiriments.txt
	)
)
echo executando o script
workon %Projeto% && pip freeze > requiriments.txt && %file%
