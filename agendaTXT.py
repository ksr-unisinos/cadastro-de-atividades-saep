from contato import *

def handleAgenda(data,df):
	valores = []
	valores.append("*******************************************")
	valores.append('dia '+data['dia']+' - '+data['horário'])
	valores.append("")
	valores.append(data['titulo'])
	valores.append("")
	valores.append(data['resumo'])
	valores.append("")
	valores.append('# [Link da Sala]('+data['link']+')')
	valores.append("")
	if len(data['participantes']) > 1:
		valores.append('### Palestrantes:')
	else: valores.append('### Palestrante:')
	emails =[]
	for i in data['participantes']:
		valores.append(i)
		valores += handleContato(i,df)
		emails += handleContatoEmail(i,df)
	valores.append("")
	valores += emails
	return valores

