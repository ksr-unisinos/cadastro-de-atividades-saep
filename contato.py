import pandas as pd
from pprint import pprint


def handleContato(nome,df):
	result =[]
	new = df[df.isin([nome]).any(axis=1)]

	'''
	Resumo do palestrante (apresentação para encarte);
	Instagram;
	Lattes;
	LinkedIn;
	'''
	if len(new) > 0:
		LinkedIn = str(new['LinkedIn'].values[0])
		Instagram = str(new['Instagram'].values[0])
		Lattes = str(new['Lattes'].values[0])
		Resumo = str(new['Resumo do palestrante (apresentação para encarte)'].values[0])

		if LinkedIn != 'nan':
			result.append('- [LinkedIn]('+ LinkedIn+')')
		
		if Instagram != 'nan':
			result.append('- [Instagram]('+ Instagram+')')
		if Lattes != 'nan':
			result.append('- [Lattes]('+ Lattes+')')
		result.append('')
		if Resumo != 'nan':
			result.append('```Sobre o Palestrante:``` '+ Resumo)
		result.append('')
	return result


def handleContatoEmail(nome,df):
	result =[]
	new = df[df.isin([nome]).any(axis=1)]
	'''
	e-mail;
	'''
	if len(new) > 0:
		email = str(new['e-mail'].values[0])
		if email != 'nan':
			result.append(email)
		result.append('')
	return result
	


def handleContatoJson(nome,df):
	result =[]
	new = df[df.isin([nome]).any(axis=1)]

	palestrante = {'nome':nome}

	if len(new) > 0:
		temp = str(new['LinkedIn'].values[0])
		if temp != 'nan':
			palestrante['LinkedIn'] = temp

		temp = str(new['Instagram'].values[0])
		if temp != 'nan':
			palestrante['Instagram'] = 'https://instagram.com/'+temp
			
		temp = str(new['e-mail'].values[0])
		if temp != 'nan':
			palestrante['e-mail'] = 'mailto:'+temp

		temp = str(new['Lattes'].values[0])
		if temp != 'nan':
			palestrante['Lattes'] = temp

		temp = str(new['Foto para divulgação'].values[0]).replace('open?','uc?')
		if temp != 'nan':
			palestrante['Foto'] = temp

		temp = str(new['Resumo do palestrante (apresentação para encarte)'].values[0])
		if temp != 'nan':
			palestrante['Resumo'] = temp
	return palestrante
