import pandas as pd
import json
from pprint import pprint
import sitePress
from contato import *
import confidencial
import docEventos
import agendaTXT


# Carrega as planilhas para o banco de palestras do arquivo "confidencial.py" que não é versionado
palestras = pd.read_excel(confidencial.driveLinkPalestras)
'''
Carimbo de data/hora;
Endereço de e-mail;
Nome do organizador;
Qual o título da atividade?;
Qual o resumo da atividade?;
Nome completo dos palestrantes (separado por virgula);
Palavras Chaves (separadas por virgula);
Horário Agendado [Segunda (17/10)];
Horário Agendado [Teça (18/10)];
Horário Agendado [Quarta (19/10)];
Horário Agendado [Quinta (20/10)];
Horário Agendado [Sexta (21/10)];
Horário Agendado [Sábado (22/10) somente das 9:30 as 11:30];
Áreas Relacionadas (Todas que achar que se relaciona com o tema e palestrantes);
É um evento em parceria com Conexão Pesquisa?;
Link curto;
'''

palestrantes = pd.read_excel(confidencial.driveLinkPalestrante)
'''
Carimbo de data/hora;
Nome Completo do Palestrante;
e-mail;
Resumo do palestrante (apresentação para encarte);
Instagram;
Lattes;
LinkedIn;
Foto para divulgação;
Deseja que a Unisinos gera certificado?;
RA - (professores e funcionários da Unisinos);
Data de Nascimento;
Estado Natal;
Local de Nascimento;
Sexo;
Endereço;
Telefone;
CPF;
Passaporte (apenas para estrangeiros)
'''
palestrasHeader = list(palestras.columns.values)

dias = []
for i in palestrasHeader:
	if 'Horário Agendado' in i:
		dias.append(i)


semana = {
	'17':'Segunda',
	'18':'Terça',
	'19':'Quarta',
	'20':'Quinta',
	'21':'Sexta',
	'22':'Sábado',
}

def handleTime(d):
	if type(d)==type('n'):
		d = d.replace(':00','h')
		d = d.replace(' às ',' - ')
		d = d.replace(':30',':30h')
		d = 'Horário: ('+d+')'
		return d
	return "nan"



calendario = []
saida = []
nomesPalestrando=[]
dataArray = []

# Verifica a quantidade
n = len(palestras['Nome do organizador'])

for i in range(n):
	dia = ''
	horario = ''
	for d in range(len(dias)):
		agenda = handleTime(palestras[dias[d]][i])
		if agenda != "nan":
			dia = str(17+d)
			horario = agenda
	
	palavra_chave = str(palestras['Palavras Chaves (separadas por virgula)'][i])
	palavra_chave = palavra_chave.replace(', ',',')
	participantes = str(palestras['Nome completo dos palestrantes (separado por virgula)'][i])
	participantes = participantes.replace(', ',',')
	nomesPalestrando += participantes.split(',')
	data = {
		"titulo": palestras['Qual o título da atividade?'][i],
		"dia": dia,
		"participantes": participantes.split(','),
		"resumo": palestras['Qual o resumo da atividade?'][i],
		"palavras-chave": palavra_chave.split(','),
		"horário": horario,
		"link": palestras["Link curto"][i],
		"Nome do Link": palestras["Nome do Link"][i],
		"semana": semana[dia],
	}
	dataArray.append(data)
	
	

	calendario += agendaTXT.handleAgenda(data,palestrantes)
	calendario.append(palestras['Endereço de e-mail'][i])
	calendario.append('')
	
	saida.append(data)

#pprint(saida)

# monta o guia da página web
sitePress.handleHtml(dataArray,palestrantes)

#pprint(calendario)
with open('agenda.txt', 'w', encoding='utf8') as f:
	for row in calendario:
		f.write(str(row) + '\n')

# Monta o json para o site da SAEP
with open('data.json', 'w', encoding='utf8') as fp:
	json.dump(saida, fp, ensure_ascii=False)

# monta o doc para copiar para o documento da unisinos
docEventos.handleDocDF(dataArray,palestrantes,semana,nomesPalestrando)
